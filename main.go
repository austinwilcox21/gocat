package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/spf13/cobra"
)

var showLineNumbers bool

var rootCmd = &cobra.Command{
	Use: "cat [SourceFile]",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("Requires at least one arg")
		}
		return nil
	},
	Short: "Print the output of a file to stdout",
	Long:  "A long description",
	Run: func(cmd *cobra.Command, args []string) {
		for i := 0; i < len(args); i++ {
			_, err := doesFileExist(args[i])
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			printFileToStdout(args[i])
		}
	},
}

func doesFileExist(fileName string) (bool, error) {
	_, err := os.Stat(os.Args[1])
	if err != nil {
		return false, err
	}

	return true, nil
}

func printFileToStdout(fileName string) {
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	var counter = 1
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if showLineNumbers {
			io.WriteString(os.Stdout, fmt.Sprintf("%d\t%s\n", counter, scanner.Text()))
		} else {
			io.WriteString(os.Stdout, fmt.Sprintf("%s\n", scanner.Text()))
		}
		counter++
	}

	if err := scanner.Err(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func main() {
	rootCmd.Flags().BoolVarP(&showLineNumbers, "numbers", "n", false, "Show Line Numbers")

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
